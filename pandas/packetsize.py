import matplotlib.pyplot as plt
from operator import itemgetter
import numpy as np 
import pandas as pd 


def pandas_analysis():
	plt.style.use('seaborn-darkgrid')
	# =================================================================================
	mqtt_single = read_single_command_file('results/data-usage/single_command_mqtt.csv')
	webhook_single = read_single_command_file('results/data-usage/single_command_webhook.csv')
	mqtt_multiple = read_multiple_command_file('results/data-usage/multiple_command_mqtt.csv')
	webhook_multiple = read_multiple_command_file('results/data-usage/multiple_command_webhook.csv')

	# =================================================================================
	print(mqtt_single.sum())
	print(webhook_single.sum())
	print(mqtt_multiple.sum())
	print(webhook_multiple.sum())

	# =================================================================================
	cumulative_df = pd.DataFrame()
	cumulative_df['HTTP'] = webhook_multiple['Webhook 100 Messages Packet Sizes'].cumsum()
	cumulative_df['MQTT'] = mqtt_multiple['MQTT 100 Messages Packet Sizes'].cumsum()


	# =================================================================================
	cplot = cumulative_df.plot.area(stacked=False)
	cplot.set(xlabel="Comandos recebidos", ylabel="Dados consumidos em bytes",xlim=(0,100),xticks=range(0,101,10))
	plt.subplots_adjust(right=0.8)
	plt.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
	plt.show()

def read_single_command_file(filename):
	df = pd.read_csv(filename, encoding='ISO-8859-1')
	return df

def read_multiple_command_file(filename):
	df = pd.read_csv(filename, encoding='ISO-8859-1')
	return df



pandas_analysis()








