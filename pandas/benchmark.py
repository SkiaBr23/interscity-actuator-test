import matplotlib.pyplot as plt
from operator import itemgetter
import numpy as np 
import pandas as pd 


def panda_analysis(file1,file2,file3):
	plt.style.use('seaborn-darkgrid')
	webhook_df = filter_new_dataframe(file2,10)
	mqtt_df = filter_new_dataframe(file3,10)
	old_df = filter_old_dataframe(file1,6,10)

	

	# =================================================================================
	# Average times, full steps

	average_all_steps_df = average_dataframe(old_df,webhook_df,mqtt_df)
	average_all_steps_df = average_all_steps_df.reindex(['Webhook V1','Webhook V2','MQTT V2'])
	print(average_all_steps_df)
	ax = average_all_steps_df.plot.barh(stacked=True)
	ax.set(ylabel="Versão da plataforma",xlabel="Tempo médio em milissegundos",xticks=range(0,71,5))
	plt.legend(['Tempo médio de criação','Tempo médio de envio','Tempo médio de entrega'],bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
	plt.subplots_adjust(right=0.7)
	plt.show()




	# =================================================================================
	# Build Total Platform Time DataFrame
	total_platform_df = pd.DataFrame()
	total_platform_df = total_platform_dataframe(old_df,webhook_df,mqtt_df)
	print(total_platform_df.describe())
	#total_platform_df = total_platform_df.rolling(5).mean()
	tp_plot = total_platform_df.plot(lw=0.9)
	tp_plot.set(ylabel="Tempo de execução total na plataforma (ms)",xlabel="Número de comandos",yticks=range(0,int(total_platform_df.max().max()+5),10))
	plt.legend(['Webhook V1','Webhook V2','MQTT V2'],bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
	plt.subplots_adjust(right=0.8)
	#plt.show()

	total_platform_mean = total_platform_df.rolling(20).mean()
	tp_mean_plot = total_platform_mean.plot()
	tp_mean_plot.set(ylabel="Tempo médio de execução na plataforma (ms)",xlabel="Número de comandos",yticks=range(10,int(total_platform_mean.max().max()+5),5))
	plt.legend(['Webhook V1','Webhook V2','MQTT V2'],bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
	plt.subplots_adjust(right=0.8)
	#plt.show()


	# =================================================================================
	# Build Total Visualized Time DataFrame
	total_visualized_df = pd.DataFrame()
	total_visualized_df = total_visualized_dataframe(old_df,webhook_df,mqtt_df)
	print(total_visualized_df.describe())
	#total_visualized_df.plot()
	#plt.show()
	# =================================================================================
	delivery_df = pd.DataFrame()
	delivery_df['Webhook V1'] = old_df['Delivery Time']
	delivery_df['Webhook V2'] = webhook_df['Delivery Time']
	delivery_df['MQTT V2'] = mqtt_df['Delivery Time']

	delivery_avg = pd.DataFrame()
	delivery_avg = delivery_average_dataframe(old_df,webhook_df,mqtt_df)
	delivery_avg_errors = errors_delivery(old_df,webhook_df,mqtt_df)
	delivery_avg_plot = delivery_avg.plot.bar(rot=0, legend=False,width=0.6,yerr=delivery_avg_errors)
	delivery_avg_plot.set(ylabel="Tempo total de entrega do comando ao dispositivo (ms)",xlabel="Número de comandos")



	plt.show()
	print(delivery_df.describe())
	delivery_mean = delivery_df.rolling(20).mean()
	te_mean_delivery=  delivery_df.iloc[10:1000].plot.line()
	te_mean_delivery.set(ylabel="Tempo total de entrega do comando ao dispositivo (ms)",xlabel="Número de comandos")
	plt.legend(['Webhook V1','Webhook V2','MQTT V2'],bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
	plt.subplots_adjust(right=0.8)
	#plt.show()


	#total_platform_df.plot.bar()
	# print(delivery_df.describe())
	# print(total_platform_df.describe())	
	# print(total_visualized_df.describe())

	# df.plot(style='.-', markevery=5)

	# =================================================================================
	# Build Average Intervals
	avg_df = average_platform_dataframe(old_df,webhook_df,mqtt_df)
	err_df = errors_platform_dataframe(old_df,webhook_df,mqtt_df)
	avg_plot = avg_df.plot.bar(rot=0, legend=False,width=0.6,yerr=err_df)

	# for p in avg_plot.patches:
	# 	avg_plot.annotate(str(p.get_height()), (p.get_x() * 1.005, p.get_height() * 1.005))
	avg_plot.set(ylabel="Tempo de execução (ms)",xlabel="Versão da plataforma",yticks=range(0,int(avg_df.max().max()+2),2))
	plt.legend(['Tempo médio de criação','Tempo médio de envio','Tempo médio de atualização'],bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
	plt.subplots_adjust(right=0.7)

	# =================================================================================


def average_dataframe(old_df,webhook_df,mqtt_df):
	avg_df = pd.DataFrame()
	avg_df['Webhook V1'] = pd.Series([old_df['Creation Time'].mean(),old_df['Sending Time'].mean(),old_df['Delivery Time'].mean()],index=['Average Creation Time','Average Sending Time', 'Average Delivery Time'])
	avg_df['Webhook V2'] = pd.Series([webhook_df['Creation Time'].mean(),webhook_df['Sending Time'].mean(),webhook_df['Delivery Time'].mean()],index=['Average Creation Time','Average Sending Time', 'Average Delivery Time'])
	avg_df['MQTT V2'] = pd.Series([mqtt_df['Creation Time'].mean(),mqtt_df['Sending Time'].mean(),mqtt_df['Delivery Time'].mean()],index=['Average Creation Time','Average Sending Time', 'Average Delivery Time'])
	return avg_df.transpose()

def delivery_average_dataframe(old_df,webhook_df,mqtt_df):
	avg_delivery = pd.DataFrame()
	avg_delivery['Webhook V1'] = pd.Series([old_df['Delivery Time'].mean()],index=['Average Delivery Time'])
	avg_delivery['Webhook V2'] = pd.Series([webhook_df['Delivery Time'].mean()],index=['Average Delivery Time'])
	avg_delivery['MQTT V2'] = pd.Series([mqtt_df['Delivery Time'].mean()],index=['Average Delivery Time'])
	return avg_delivery.transpose()

def errors_dataframe(old_df,webhook_df,mqtt_df):
	err_df = pd.DataFrame()
	err_df['Webhook V1'] = pd.Series([old_df['Creation Time'].std(),old_df['Sending Time'].std(),old_df['Update Time'].std(),old_df['Delivery Time'].std()],index=['Average Creation Time','Average Sending Time', 'Average Update Time', 'Average Delivery Time'])
	err_df['Webhook V2'] = pd.Series([webhook_df['Creation Time'].std(),webhook_df['Sending Time'].std(),webhook_df['Update Time'].mean(),webhook_df['Delivery Time'].mean()],index=['Average Creation Time','Average Sending Time', 'Average Update Time', 'Average Delivery Time'])
	err_df['MQTT V2'] = pd.Series([mqtt_df['Creation Time'].std(),mqtt_df['Sending Time'].std(),mqtt_df['Update Time'].std(),mqtt_df['Delivery Time'].std()],index=['Average Creation Time','Average Sending Time', 'Average Update Time', 'Average Delivery Time'])
	return err_df.transpose()

def average_platform_dataframe(old_df,webhook_df,mqtt_df):
	avg_df = pd.DataFrame()
	avg_df['Webhook V1'] = pd.Series([old_df['Creation Time'].mean(),old_df['Sending Time'].mean(),old_df['Update Time'].mean()],index=['Average Creation Time','Average Sending Time', 'Average Update Time'])
	avg_df['Webhook V2'] = pd.Series([webhook_df['Creation Time'].mean(),webhook_df['Sending Time'].mean(),webhook_df['Update Time'].mean()],index=['Average Creation Time','Average Sending Time', 'Average Update Time'])
	avg_df['MQTT V2'] = pd.Series([mqtt_df['Creation Time'].mean(),mqtt_df['Sending Time'].mean(),mqtt_df['Update Time'].mean()],index=['Average Creation Time','Average Sending Time', 'Average Update Time'])
	return avg_df.transpose()

def errors_delivery(old_df,webhook_df,mqtt_df):
	err_df = pd.DataFrame()
	err_df['Webhook V1'] = pd.Series([old_df['Delivery Time'].std()],index=['Average Delivery Time'])
	err_df['Webhook V2'] = pd.Series([webhook_df['Delivery Time'].std()],index=['Average Delivery Time'])
	err_df['MQTT V2'] = pd.Series([mqtt_df['Delivery Time'].std()],index=['Average Delivery Time'])

	return err_df.transpose()

def errors_platform_dataframe(old_df,webhook_df,mqtt_df):
	err_df = pd.DataFrame()
	err_df['Webhook V1'] = pd.Series([old_df['Creation Time'].std(),old_df['Sending Time'].std(),old_df['Update Time'].std()],index=['Average Creation Time','Average Sending Time', 'Average Update Time'])
	err_df['Webhook V2'] = pd.Series([webhook_df['Creation Time'].std(),webhook_df['Sending Time'].std(),webhook_df['Update Time'].std()],index=['Average Creation Time','Average Sending Time', 'Average Update Time'])
	err_df['MQTT V2'] = pd.Series([mqtt_df['Creation Time'].std(),mqtt_df['Sending Time'].std(),mqtt_df['Update Time'].std()],index=['Average Creation Time','Average Sending Time', 'Average Update Time'])
	return err_df.transpose()


def total_platform_dataframe(old_df,webhook_df,mqtt_df):
	total_df = pd.DataFrame()
	total_df['Webhook V1 Total Platform Time'] = old_df['Creation Time'] + old_df['Sending Time'] + old_df['Update Time']
	total_df['Webhook V2 Total Platform Time'] = webhook_df['Creation Time'] + webhook_df['Sending Time'] + webhook_df['Update Time']
	total_df['MQTT V2 Total Platform Time'] = mqtt_df['Creation Time'] + mqtt_df['Sending Time'] + mqtt_df['Update Time']
	return total_df

def total_visualized_dataframe(old_df,webhook_df,mqtt_df):
	total_df = pd.DataFrame()

	total_df['Webhook V1 Total Visualized Time'] = old_df['Creation Time'] + old_df['Sending Time'] + old_df['Delivery Time']
	total_df['Webhook V2 Total Visualized Time'] = webhook_df['Creation Time'] + webhook_df['Sending Time'] + webhook_df['Delivery Time']
	total_df['MQTT V2 Total Visualized Time'] = mqtt_df['Creation Time'] + mqtt_df['Sending Time'] + mqtt_df['Delivery Time']

	return total_df


def filter_new_dataframe(filename,delay):
	df = pd.read_csv(filename, encoding='ISO-8859-1')

	df['Creation Time'] = (df['Creation Time']*1000)
	df['Sending Time'] = (df['Sending Time']*1000)
	df['Update Time'] = (df['Update Time']*1000)
	df['Delivery Time'] = ((df['Delivery Time'])*1000) + delay
	return df

def filter_old_dataframe(filename,delay1,delay2):
	df = pd.read_csv(filename, encoding='ISO-8859-1')

	df['Creation Time'] = (df['Creation Time']*1000)
	df['Sending Time'] = (df['Sending Time']*1000) + delay1
	df['Update Time'] = (df['Update Time']*1000)
	df['Delivery Time'] = ((df['Delivery Time'])*1000) + delay2
	return df



panda_analysis('results/benchmark/results_old.csv','results/benchmark/results_w.csv','results/benchmark/results_m.csv')








