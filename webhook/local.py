from flask import Flask, request
import json

app = Flask(__name__)

@app.route('/command',methods=['POST'])
def foo():
   data = json.loads(request.data)
   print "New command:"
   print json.dumps(data,indent=4)
   return "OK"

if __name__ == '__main__':
   app.run(host="localhost",port=7000)
