from flask import Flask, request
import json
import time

app = Flask(__name__)

@app.route('/command',methods=['POST'])
def foo():
   data = json.loads(request.data)
   timestamp = "%7f" % time.time()
   print("Benchmark::DEVICE;Command Delivered;"+timestamp+";"+data['command']['_id']['$oid']+";"+"Webhook")
 #  print "New command:"
 #  print json.dumps(data,indent=4)
   return "OK"

if __name__ == '__main__':
   app.run(host="172.31.43.41",port=7000)
