import paho.mqtt.client as mqtt
import time
import json

def on_connect(client, userdata, flags, rc):
    print("Connecting")
    client.subscribe("actuator/741e6a5b-bacb-405c-bfeb-d84c7e1e2805/#")

def on_message(client, userdata, msg):
	timestamp = "%7f" % time.time()
	data = json.loads(msg.payload)
   	print("Benchmark::DEVICE;Command Delivered;"+timestamp+";"+data['_id']['$oid']+";"+"MQTT")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("18.222.30.221", 1883, 60)

client.loop_forever()
